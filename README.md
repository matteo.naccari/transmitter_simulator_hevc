Transmitter simulator for bitstreams compliant with the H.265/HEVC standard

Please see the `transmitter_simulator_hevc_user_manual.pdf` for more information.
